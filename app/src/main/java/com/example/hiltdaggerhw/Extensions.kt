package com.example.hiltdaggerhw

fun String?.orNA(): String = if (isNullOrEmpty()) "n/a" else this