package com.example.hiltdaggerhw.viewmodel.vmfactory

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.hiltdaggerhw.model.MovieListRepo
import com.example.hiltdaggerhw.viewmodel.MovieListViewModel

class MovieListVMFactory(
    private val repo: MovieListRepo
) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return MovieListViewModel(repo) as T
    }
}

