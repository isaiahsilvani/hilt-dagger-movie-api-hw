package com.example.hiltdaggerhw.viewmodel

import android.util.Log
import android.view.View
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.hiltdaggerhw.model.MovieListRepo
import com.example.hiltdaggerhw.view.MovieListFragment.MovieListState
import dagger.hilt.android.scopes.ViewModelScoped
import kotlinx.coroutines.launch
import javax.inject.Inject


class MovieListViewModel(private val repo: MovieListRepo) : ViewModel() {

    private val _movieListState: MutableLiveData<MovieListState> = MutableLiveData(MovieListState())
    val movieListState: LiveData<MovieListState> get() = _movieListState

    fun getMovies(movieName: String) {
        viewModelScope.launch {
            viewModelScope.launch {
                with(_movieListState) {
                    value = MovieListState(isLoading = true)
                    val movies = repo.getMovies(movieName)
                    if (movies.isNotEmpty()) {
                        value = value?.copy(movieList = movies, response = true)
                        value = value?.copy(isLoading = false)
                    } else {
                        value = MovieListState(movieList = movies, response = false)
                    }
                }
            }
        }
    }

    fun restoreMovies() {
        viewModelScope.launch {
            with(_movieListState) {
                value = MovieListState(isLoading = true)
                value = value?.copy(movieList = repo.restoreMovies())
                value = value?.copy(isLoading = false)
            }
        }
    }
}