package com.example.hiltdaggerhw.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.hiltdaggerhw.model.MovieDetails
import com.example.hiltdaggerhw.model.MovieListRepo
import com.example.hiltdaggerhw.model.Rating
import com.example.hiltdaggerhw.view.MovieDetailFragment.MovieDetailState
import com.example.hiltdaggerhw.view.MovieListFragment.MovieListState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject
import kotlin.reflect.full.memberProperties


class MovieDetailViewModel @Inject constructor(private val repo: MovieListRepo) : ViewModel() {

    private val _movieDetailsState: MutableLiveData<MovieDetailState> =
        MutableLiveData(MovieDetailState())
    val movieDetailsState: LiveData<MovieDetailState> get() = _movieDetailsState

    fun getMovieDetails(movieId: String) {
        viewModelScope.launch {
            with (_movieDetailsState) {
                value = MovieDetailState(isLoading = true)
                value = MovieDetailState(repo.getMovieDetails(movieId))
            }
        }
    }
}
