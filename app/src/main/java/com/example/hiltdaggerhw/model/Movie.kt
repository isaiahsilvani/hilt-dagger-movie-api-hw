package com.example.hiltdaggerhw.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Movie(
    @PrimaryKey
    val Poster: String,
    val Title: String,
    val Type: String,
    val Year: String,
    val imdbID: String
)