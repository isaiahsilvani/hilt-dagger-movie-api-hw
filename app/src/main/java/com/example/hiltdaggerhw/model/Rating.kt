package com.example.hiltdaggerhw.model

data class Rating(
    val Source: String,
    val Value: String
)