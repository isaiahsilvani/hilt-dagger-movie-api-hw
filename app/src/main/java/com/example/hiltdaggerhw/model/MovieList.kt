package com.example.hiltdaggerhw.model

data class MovieList(
    val Search: List<Movie>,
    val Response: String
)