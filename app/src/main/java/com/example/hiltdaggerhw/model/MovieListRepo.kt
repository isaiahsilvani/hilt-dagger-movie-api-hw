package com.example.hiltdaggerhw.model

import com.example.hiltdaggerhw.model.local.MovieDao
import com.example.hiltdaggerhw.model.remote.MovieService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject


class MovieListRepo @Inject constructor(
    private val movieService: MovieService,
    private val movieDao: MovieDao
) {

    suspend fun getMovies(movieName: String): List<Movie> = withContext(Dispatchers.IO) {
        // First, get all movies from database currently.
        val result = movieService.getMovies(s = movieName)
        val response = result.Response.lowercase().toBooleanStrict()
        if (response) {
            movieDao.deleteMovies()
            movieDao.insertMovie(*result.Search.toTypedArray())
            return@withContext result.Search
        } else {
            return@withContext emptyList()
        }
        // If response is false, return an empty list

    }

    suspend fun getMovieDetails(movieId: String) : MovieDetails = withContext(Dispatchers.IO) {
        return@withContext movieService.getMovieDetails(i = movieId)
    }

    suspend fun restoreMovies() = withContext(Dispatchers.IO){
        return@withContext movieDao.getMovies()
    }

}