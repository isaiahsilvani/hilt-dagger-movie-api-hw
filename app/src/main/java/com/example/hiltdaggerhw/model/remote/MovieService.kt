package com.example.hiltdaggerhw.model.remote

import com.example.hiltdaggerhw.model.MovieDetails
import com.example.hiltdaggerhw.model.MovieList
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.create
import retrofit2.http.GET
import retrofit2.http.Query

interface MovieService {

    companion object {
        // Endpoints as static variables
        private const val BASE_URL = "https://www.omdbapi.com"
        // http://www.omdbapi.com/?s=Batman&apikey=e9e9c3d2
        private const val END_POINT = "/"
        private const val API_KEY = "e9e9c3d2"

        private fun myHttpClient(): OkHttpClient {
            val builder = OkHttpClient.Builder()
                .addInterceptor(MovieInterceptor())
            return builder.build()
        }


        // Get an instance by creating a Retrofit instance and using Builder
        fun getInstance(): MovieService = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .client(myHttpClient())
            .build()
            .create()
    }
    // Get movies based on a name user provides
    @GET(END_POINT)
    suspend fun getMovies(
        @Query("s") s: String
    ): MovieList

    // Get movie details with provided id
    @GET(END_POINT)
    suspend fun getMovieDetails(
        @Query("i") i: String
    ): MovieDetails

}