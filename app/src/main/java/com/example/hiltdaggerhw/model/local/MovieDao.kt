package com.example.hiltdaggerhw.model.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.hiltdaggerhw.model.Movie

@Dao
interface MovieDao {

    // Insert unlimited # of characters from a request
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertMovie(vararg movie: Movie)

    // Get ALL movies stored in database
    @Query("SELECT * FROM movie")
    suspend fun getMovies() : List<Movie>

    // Delete ALL movies stored in database
    @Query("DELETE FROM movie")
    suspend fun deleteMovies()

}