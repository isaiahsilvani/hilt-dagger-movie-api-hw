package com.example.hiltdaggerhw.model.remote

import okhttp3.HttpUrl
import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response


class MovieInterceptor : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {

        var request: Request = chain.request()

        var url: HttpUrl = request.url.newBuilder().addQueryParameter("apikey", "e9e9c3d2").build()
        request = request.newBuilder().url(url).build()

        return chain.proceed(request)
    }
}