package com.example.hiltdaggerhw.view.MovieDetailFragment

import com.example.hiltdaggerhw.model.MovieDetails

data class MovieDetailState(
    val movieDetails: MovieDetails? = MovieDetails(),
    var isLoading: Boolean = false
)

