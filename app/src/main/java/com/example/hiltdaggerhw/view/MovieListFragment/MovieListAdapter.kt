package com.example.hiltdaggerhw.view.MovieListFragment

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.navigation.Navigation
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.example.hiltdaggerhw.databinding.ItemMovieBinding
import com.example.hiltdaggerhw.model.Movie
import com.example.hiltdaggerhw.viewmodel.MovieListViewModel
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject


class MovieListAdapter(
    val navigateToDetailPage: (id: String) -> Unit
) : RecyclerView.Adapter<MovieListAdapter.MovieViewHolder>() {

    private var movieList: MutableList<Movie> = mutableListOf()

    inner class MovieViewHolder(private val binding: ItemMovieBinding) :
        RecyclerView.ViewHolder(binding.root) {
        var movieName = binding.tvMovieName
        var movieYear = binding.tvMovieYear
        var itemMovie = binding.itemMovie
        private fun displayImage(image: String) {
            binding.imgMovie.load(image)
        }

        fun displayItem(movie: Movie) {
            movieName.text = movie.Title
            movieYear.text = movie.Year
            displayImage(movie.Poster)
            itemMovie.setOnClickListener {
                    navigateToDetailPage(movie.imdbID)
            }
        }
    }

    fun setData(list: List<Movie>) {
        val oldSize = movieList.size
        movieList.clear()
        notifyItemRangeRemoved(0, oldSize)
        movieList.addAll(list)
        notifyItemRangeInserted(0, movieList.size)

    }

    // Tell the adapter what layout you're using for the viewHolder
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieViewHolder {
        return MovieViewHolder(
            ItemMovieBinding.inflate(LayoutInflater.from(parent.context))
        )
    }

    // Actually pass relevant data to the item view
    override fun onBindViewHolder(holder: MovieViewHolder, position: Int) {
        holder.displayItem(movieList[position])
    }

    override fun getItemCount(): Int {
        return movieList.size
    }
}