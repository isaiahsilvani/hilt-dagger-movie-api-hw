package com.example.hiltdaggerhw.view.MovieListFragment

import com.example.hiltdaggerhw.model.Movie

data class MovieListState(

    var movieList: List<Movie> = listOf(),
    var isLoading: Boolean = false,
    var response: Boolean? = null

)