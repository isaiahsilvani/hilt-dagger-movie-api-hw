package com.example.hiltdaggerhw.view.MovieListFragment

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SearchView
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.hiltdaggerhw.databinding.FragmentMovieListBinding
import com.example.hiltdaggerhw.viewmodel.MovieListViewModel
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class MovieListFragment : Fragment() {

    lateinit var binding: FragmentMovieListBinding

    @Inject
    lateinit var viewModel: MovieListViewModel

    private val movieAdapter by lazy {
        MovieListAdapter(this::navigateToDetailPage)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentMovieListBinding.inflate(layoutInflater)
        setRecyclerView()
        viewModel.restoreMovies()
        initObservers()
        return binding.root
    }

    private fun initObservers() {
        viewModel.movieListState.observe(viewLifecycleOwner) { state ->
            Log.e("FRAG", state.movieList.toString())

            binding.progressBar.isVisible = state.isLoading

            if (state.movieList.isNotEmpty() && state.isLoading) {
                movieAdapter.setData(state.movieList)
            }

            if (state.response == false) {
                Toast.makeText(requireContext(), "You're being stupid dude", Toast.LENGTH_LONG).show()
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        with(binding) {

            etSearchField.isSubmitButtonEnabled = true
            etSearchField.setOnClickListener {
                etSearchField.onActionViewExpanded()
            }

            etSearchField.setOnQueryTextListener(object : SearchView.OnQueryTextListener{
                override fun onQueryTextSubmit(query: String?): Boolean {
                    if (!query.isNullOrEmpty()) {
                        viewModel.getMovies(query)
                    }
                    return false
                }

                var oldString: String = ""
                // Callback whenever the text changes in SearchView
                override fun onQueryTextChange(newText: String?): Boolean {
                    // Don't wanna make suggestion to the API :/
                    return false
                }

            })

        }
    }

    private fun navigateToDetailPage(id: String) {
        val action = MovieListFragmentDirections.actionMovieListFragmentToMovieDetailFragment(id)
        findNavController().navigate(action)
    }

    private fun setRecyclerView() {
        with(binding) {
            rvMovieList.adapter = movieAdapter
            rvMovieList.layoutManager = GridLayoutManager(this@MovieListFragment.context, 2)
        }
    }
}