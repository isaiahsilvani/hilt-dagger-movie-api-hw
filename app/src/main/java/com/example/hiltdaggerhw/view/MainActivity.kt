package com.example.hiltdaggerhw.view

import androidx.appcompat.app.AppCompatActivity
import com.example.hiltdaggerhw.R
import dagger.hilt.android.AndroidEntryPoint

// Once Hilt is set up in your Application class and an application-level component is available,
// Hilt can provide dependencies to other Android classes that have the @AndroidEntryPoint annotation:
@AndroidEntryPoint
class MainActivity : AppCompatActivity(R.layout.activity_main)

// Hilt currently supports the following Android classes:

//Application (by using @HiltAndroidApp)
//ViewModel (by using @HiltViewModel)
//Activity
//Fragment
//View
//Service
//BroadcastReceiver