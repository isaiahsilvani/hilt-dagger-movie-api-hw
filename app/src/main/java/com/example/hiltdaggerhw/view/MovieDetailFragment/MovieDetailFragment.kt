package com.example.hiltdaggerhw.view.MovieDetailFragment

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import coil.load
import com.example.hiltdaggerhw.databinding.FragmentMovieDetailBinding
import com.example.hiltdaggerhw.orNA
import com.example.hiltdaggerhw.viewmodel.MovieDetailViewModel
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class MovieDetailFragment : Fragment() {

    lateinit var binding: FragmentMovieDetailBinding

    val args by navArgs<MovieDetailFragmentArgs>()

    @Inject
    lateinit var viewModel: MovieDetailViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentMovieDetailBinding.inflate(layoutInflater)
        initObservers()
        viewModel.getMovieDetails(args.movieId)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        binding.btnGoBack.setOnClickListener {
            val action =
                MovieDetailFragmentDirections.actionMovieDetailFragmentToMovieListFragment()
            findNavController().navigate(action)
        }
    }

    private fun initObservers() {
        viewModel.movieDetailsState.observe(viewLifecycleOwner) { state ->
            with(binding) {

                progressBar.isVisible = state.isLoading

                state.movieDetails?.run {
                    Log.e("DETAIL", Actors.orNA())
                    imgPoster.load(Poster.orEmpty())
                    tvDetailName.text = Title.orNA()
                    tvAwards.text = Awards.orNA()
                    tvCountry.text = Country.orNA()
                    tvGenre.text = Genre.orNA()
                    tvPlot.text = Plot.orNA()
                    tvDirector.text = Director.orNA()
                    tvRated.text = Rated.orNA()
                    tvWriter.text = Writer.orNA()
                    tvYear.text = Year.orNA()
                }
            }
        }
    }
}