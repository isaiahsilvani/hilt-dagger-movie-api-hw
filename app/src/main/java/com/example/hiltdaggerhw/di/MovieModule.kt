package com.example.hiltdaggerhw.di

import android.content.Context
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.get
import com.example.hiltdaggerhw.model.MovieListRepo
import com.example.hiltdaggerhw.model.local.MovieDB
import com.example.hiltdaggerhw.model.local.MovieDao
import com.example.hiltdaggerhw.model.remote.MovieService
import com.example.hiltdaggerhw.view.MovieListFragment.MovieListAdapter
import com.example.hiltdaggerhw.view.MovieListFragment.MovieListFragment
import com.example.hiltdaggerhw.viewmodel.MovieDetailViewModel
import com.example.hiltdaggerhw.viewmodel.MovieListViewModel
import com.example.hiltdaggerhw.viewmodel.vmfactory.MovieDetailVMFactory
import com.example.hiltdaggerhw.viewmodel.vmfactory.MovieListVMFactory
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.android.scopes.ViewModelScoped
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class MovieModule {
    // View Model
    @Provides
    fun providesMovieListViewModel(
        vmFactory: MovieListVMFactory
    ): MovieListViewModel = vmFactory.create(MovieListViewModel::class.java)

    // Repo
    @Singleton
    @Provides
    fun providesRepo(service: MovieService, dao: MovieDao): MovieListRepo =
        MovieListRepo(service, dao)

    @Provides
    fun providesMovieDetailVMFactory(repo: MovieListRepo): MovieDetailVMFactory =
        MovieDetailVMFactory(repo)

    // VM Factory
    @Provides
    @Singleton
    fun providesMovieListVMFactory(repo: MovieListRepo): MovieListVMFactory =
        MovieListVMFactory(repo)

    // Service
    @Provides
    @Singleton
    fun getMovieService(): MovieService = MovieService.getInstance()

    // Dao
    @Provides
    @Singleton
    fun getDao(movieDB: MovieDB): MovieDao = movieDB.movieDao()

    // Room Database
    @Provides
    @Singleton
    fun getDatabase(@ApplicationContext context: Context): MovieDB = MovieDB.getInstance(context)
}

// NOTE: EVERY OBJECT THAT WE'RE GOING TO CREATE HAPPENS INSIDE THIS MODULE!!!!